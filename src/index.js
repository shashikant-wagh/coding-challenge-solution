import ReactDOM from "react-dom";
import "./index.css";
import state from "./store";
import App from "./App";
import { Provider } from "react-redux";
import { setVideoData } from "./store/video.actions";

state.dispatch(setVideoData());

ReactDOM.render(
  <Provider store={state}>
    <App />
  </Provider>,
  document.getElementById("root")
);
