import React from "react";
import { Link } from "react-router-dom";
import { useSelector } from "react-redux";
import { getCurrentVideoSource } from "../store/video.selector";

export default function VideoThumbnail({ video }) {
  // get current video
  const currentVideo = useSelector(getCurrentVideoSource);

  return (
    <Link
      to={video.route}
      data-testid="videoThumbnail"
      className="text-decoration-none text-dark"
    >
      <div
        className={`d-flex mb-4 flex-lg-column ${
          currentVideo.route === video.route && "border border-danger p-3"
        }`}
      >
        <img alt="name" className="thumbnail-sm" src={video.thumbnail} />
        <h6 className="text-truncate ps-4 ps-lg-0 mt-0 mt-lg-1">
          {video.name}
        </h6>
      </div>
    </Link>
  );
}
