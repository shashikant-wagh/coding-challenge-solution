import {
  Player,
  ControlBar,
  ReplayControl,
  ForwardControl,
  CurrentTimeDisplay,
  TimeDivider,
  PlaybackRateMenuButton,
  VolumeMenuButton,
  BigPlayButton,
} from "video-react";
import { useEffect } from "react";
import HLSSource from "./HLSSource";
import { useLocation } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import { getCurrentVideoSource } from "../store/video.selector";
import { setDefaultSelectedVideo } from "../store/video.actions";

export default function VideoRectangle() {
  const dispatch = useDispatch();

  // get current video.
  const { pathname } = useLocation();
  const video = useSelector(getCurrentVideoSource);

  useEffect(() => {
    // update default video from url.
    const slug = pathname.slice(1);
    dispatch(setDefaultSelectedVideo(slug));
  }, [pathname, dispatch]);

  return (
    <>
      <h4 className="text-truncate mb-2"> {video.name} </h4>
      <Player
        poster={video.thumbnail}
        playsInline
        fluid={false}
        width={"100%"}
        height={500}
      >
        <BigPlayButton position="center" />
        <HLSSource isVideoChild src={video.path} />
        <ControlBar>
          <ReplayControl seconds={10} order={1.1} />
          <ForwardControl seconds={30} order={1.2} />
          <CurrentTimeDisplay order={4.1} />
          <TimeDivider order={4.2} />
          <PlaybackRateMenuButton rates={[5, 2, 1, 0.5, 0.1]} order={7.1} />
          <VolumeMenuButton disabled />
        </ControlBar>
      </Player>
    </>
  );
}
