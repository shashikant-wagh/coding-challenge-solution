import VideoThumbnail from "./VideoThumbnail";
import { useSelector } from "react-redux";
import { getVideoList } from "../store/video.selector";

export default function VideoList() {
  const videos = useSelector(getVideoList);

  return videos.map((video) => {
    return <VideoThumbnail key={`${video.name}-thumbnail`} video={video} />;
  });
}
