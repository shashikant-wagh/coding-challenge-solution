import VideoList from "./VideoList";
import { Link } from "react-router-dom";
import VideoRectangle from "./VideoRectangle";

export default function VideoPlayer() {
  return (
    <>
      <div className="py-5 text-center">
        <Link to="/" className="d-inline-block text-decoration-none text-dark">
          <h2>Video Player</h2>
        </Link>
        <p className="lead">A video player to stream your videos.</p>
      </div>
      <div className="row g-5">
        <div className="col-12 col-lg-8" data-testid="currentVideo">
          <VideoRectangle />
        </div>
        <div className="col-12 col-lg-3 ms-auto overflow-auto">
          <div className="flex-grow-1 h-0">
            <VideoList />
          </div>
        </div>
      </div>
    </>
  );
}
