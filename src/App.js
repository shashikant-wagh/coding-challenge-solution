import React from "react";
import "./App.css";
import "video-react/dist/video-react.css";
import VideoPlayer from "./components/VideoPlayer";
import { BrowserRouter as Router, Routes, Route } from "react-router-dom";

import "bootstrap/dist/js/bootstrap.min.js";
import "bootstrap/dist/css/bootstrap.min.css";

function App() {
  return (
    <div className="App container">
      <Router>
        <Routes>
          <Route path="/*" element={<VideoPlayer />} />
        </Routes>
      </Router>
    </div>
  );
}

export default App;
