import store from "./store";
import { Provider } from "react-redux";
import { render } from "@testing-library/react";
import VideoPlayer from "./components/VideoPlayer";
import { BrowserRouter as Router, Routes, Route } from "react-router-dom";

window.Hls = jest.fn(() => true);

function ReduxProvider({ children }) {
  window.history.pushState({}, "", children?.route || "/");

  return (
    <Provider store={store}>
      <Router>
        <Routes>
          <Route path="/*" element={<VideoPlayer />} />
        </Routes>
      </Router>
    </Provider>
  );
}

const reduxRender = (ui, options = []) =>
  render(ui, { wrapper: ReduxProvider }, ...options);

export * from "@testing-library/react";
export { reduxRender as render };
