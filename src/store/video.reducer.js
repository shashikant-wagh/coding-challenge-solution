import ActionTypes from "./video.type";

const initialState = {
  player: {
    videoSelected: 0,
    videos: [],
  },
};

const videoReducer = (state = initialState, action) => {
  switch (action.type) {
    case ActionTypes.HYDRATE_VIDEO_DATA:
      return {
        ...state,
        player: {
          ...state.player,
          videos: action.payload,
        },
      };

    case ActionTypes.SELECT_VIDEO:
      return {
        ...state,
        player: {
          ...state.player,
          videoSelected: action.payload,
        },
      };

    default:
      return state;
  }
};

export default videoReducer;
