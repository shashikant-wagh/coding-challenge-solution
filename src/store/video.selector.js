export const getVideoList = ({ player }) => Object.values(player.videos);
export const getCurrentVideoSource = ({ player }) => player.videoSelected;
