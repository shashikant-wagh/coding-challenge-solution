import ActionTypes from "./video.type";
import videoList from "../video-data.json";

const toSlug = (str) => {
  return str.toLowerCase().replaceAll(" ", "-");
};

const sortVidoes = (videos) => {
  return videos.slice().sort((prev, next) => {
    if (prev.name > next.name) {
      return 1;
    } else if (prev.name < next.name) {
      return -1;
    } else {
      return 0;
    }
  });
};

const normalizedVideoRecords = (videos) => {
  return sortVidoes(videos).reduce((records, video) => {
    const slug = toSlug(video.name);

    return {
      ...records,
      [slug]: { ...video, route: `/${slug}` },
    };
  }, {});
};

export const setVideoData = () => {
  return {
    type: ActionTypes.HYDRATE_VIDEO_DATA,
    payload: normalizedVideoRecords(videoList.videos),
  };
};

export const setSelectedVideo = (video) => {
  return {
    type: ActionTypes.SELECT_VIDEO,
    payload: video,
  };
};

export const setDefaultSelectedVideo = (slug) => {
  const videos = normalizedVideoRecords(videoList.videos);

  return {
    type: ActionTypes.SELECT_VIDEO,
    payload: slug ? videos[slug] : Object.values(videos)[0],
  };
};
