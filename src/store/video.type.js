const actions = {
    HYDRATE_VIDEO_DATA: 'HYDRATE_VIDEO_DATA',
    SELECT_VIDEO: 'SELECT_VIDEO'
}

export default actions
