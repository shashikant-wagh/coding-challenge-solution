import { render } from "../test.utils";
import { screen } from "@testing-library/react";
import store from "../store";
import { setVideoData } from "../store/video.actions";
import { getVideoList } from "../store/video.selector";

const setup = () => render();

describe("Sorted List", () => {
  it("renders sorted list of video list with title", () => {
    setup();
    store.dispatch(setVideoData());

    const videos = getVideoList(store.getState());
    const videoTitles = videos.map(({ name }) => name);

    const videoTitlesFromPage = screen
      .getAllByTestId("videoThumbnail")
      .map((el) => el.getElementsByTagName("h6")[0].textContent);

    expect(videoTitlesFromPage).toStrictEqual(videoTitles);
  });

  it("renders sorted list of video list with thumbnails", () => {
    setup();
    store.dispatch(setVideoData());

    const videos = getVideoList(store.getState());
    const videoThumbnails = videos.map(({ thumbnail }) => thumbnail);

    const videoThumbnailsFromPage = screen
      .getAllByTestId("videoThumbnail")
      .map((el) => el.getElementsByTagName("img")[0].src);

    expect(videoThumbnailsFromPage).toStrictEqual(videoThumbnails);
  });
});
