import store from "../../store";
import { render } from "../../test.utils";
import { screen } from "@testing-library/react";
import { setVideoData } from "../../store/video.actions";
import userEvent from "@testing-library/user-event";
import { getVideoList } from "../../store/video.selector";

const setup = async (videoNo = 1, route = "/", performClick = true) => {
  render({ route });

  store.dispatch(setVideoData());
  const videoThumbnailEl = screen.getAllByTestId("videoThumbnail")[videoNo];
  if (performClick) await userEvent.click(videoThumbnailEl);
  return videoThumbnailEl;
};

describe("Interaction", () => {
  it("on click it changes current video", async () => {
    const videoThumbnailEl = await setup();

    const currentVideoText = screen
      .getByTestId("currentVideo")
      .getElementsByTagName("h4")[0]
      .textContent.trim();

    expect(videoThumbnailEl.textContent).toStrictEqual(currentVideoText);
  });

  it("on click it sets updates url for current video", async () => {
    const videoNo = 2;
    await setup(videoNo);
    const videos = getVideoList(store.getState());
    expect(window.location.pathname).toStrictEqual(videos[videoNo].route);
  });

  it("on click it sets updates url for current video", async () => {
    const videoNo = 2;
    await setup(undefined, "/about-bananas", false);
    const videos = getVideoList(store.getState());

    const currentVideoText = screen
      .getByTestId("currentVideo")
      .getElementsByTagName("h4")[0]
      .textContent.trim();

    expect(currentVideoText).toStrictEqual(videos[videoNo - 1].name);
  });
});
