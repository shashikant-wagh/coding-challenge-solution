import store from "../store";
import { render } from "../test.utils";
import { screen } from "@testing-library/react";
import { setVideoData } from "../store/video.actions";
import { getCurrentVideoSource } from "../store/video.selector";

const setup = () => render();

describe("Current Video", () => {
  it("renders video element", () => {
    setup();

    const isVideoLoaded = !!screen
      .getByTestId("currentVideo")
      .getElementsByTagName("video").length;

    expect(isVideoLoaded).toBeTruthy();
  });

  it("renders first title from video list by default", () => {
    setup();
    store.dispatch(setVideoData());

    const { name } = getCurrentVideoSource(store.getState());

    const curerntVideoTitleFromPage = screen
      .getByTestId("currentVideo")
      .getElementsByTagName("h4")[0]
      .textContent.trim();

    expect(curerntVideoTitleFromPage).toStrictEqual(name);
  });
});
